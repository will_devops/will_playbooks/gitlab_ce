Gitlab CE  
=========  
  
A simple ansible role for Gitlab CE instalation
  
Requirements  
------------  
  
Ansible installed  
  
Default Variables  
--------------  
  
 None 

Dependencies  
------------  
  
None  
  
Example Playbook  
----------------  
  
    - hosts: servers  
      roles:  
         - { role: gitlab_ce }  
  
License  
-------  
  
BSD  